FROM debian:12-slim
WORKDIR /repo
COPY run.sh /root
RUN apt update && apt install createrepo-c -y
CMD /root/run.sh