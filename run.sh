#!/bin/bash
set -e
while :
do
    createrepo_c /repo
    sleep $(($DELAY * 60))
done