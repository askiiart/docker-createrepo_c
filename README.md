# docker-createrepo_c

[![Build Status](https://drone.askiiart.net/api/badges/askiiart/docker-createrepo_c/status.svg?ref=refs/heads/main)](https://drone.askiiart.net/askiiart/docker-createrepo_c)

`createrepo_c` in a Docker container

## Running

Example `docker run`:

```bash
docker run -d -e DELAY=5 -v /path/to/repo:/repo docker.askiiart.net/askiiart/createrepo_c
```

Example `docker-compose.yml`:

```yaml
version: '3.7'
services:
  hugo:
    image: docker.askiiart.net/askiiart/createrepo_c
    environment:
      - DELAY=5
    volumes:
      - /path/to/repo:/repo
```

## Building

1. Run `docker build .`
