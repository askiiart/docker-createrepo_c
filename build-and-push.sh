#!/usr/bin/env bash
set -e
ORG=askiiart
NAME=createrepo_c
ID=$(docker build . -q)

docker tag ${ID} ${ORG}/${NAME}:latest
docker push ${ORG}/${NAME}:latest
